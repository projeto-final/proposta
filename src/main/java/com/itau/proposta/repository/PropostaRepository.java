package com.itau.proposta.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.proposta.model.Proposta;

public interface PropostaRepository extends CrudRepository<Proposta, Integer>{
	Proposta findById(int id);
	Proposta findByCpfCliente(String cpfCliente);
	Proposta findByCpfClienteAndStatus(String cpfCliente, boolean status);
}

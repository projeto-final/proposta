package com.itau.proposta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.proposta.model.Proposta;
import com.itau.proposta.repository.PropostaRepository;

@Controller
public class PropostaController {
	@Autowired
	PropostaRepository propostaRepository;
	
	@RequestMapping(path="/proposta/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Proposta getPropostaById(@PathVariable int id) {	
		
		return propostaRepository.findById(id);
	}
	
// Exemplo de chamada do JSON!	
//	{
//		"nomeCliente": "Filipe",
//		"cpfCliente": "12345678910",
//		"idade": 28,
//		"produto": 28,
//		"produtoNome": "Seguro Itaú Vida",
//		"valorMensal": 128
//		
//	}
	
	@RequestMapping(path="/proposta/salvar", method=RequestMethod.POST)
	@ResponseBody
	@CrossOrigin
	public ResponseEntity<?>  salvarProposta(@RequestBody Proposta proposta) 
	{
		Proposta validaProposta = propostaRepository.findByCpfClienteAndStatus(proposta.getCpfCliente(), false);
		if (validaProposta != null) {
			proposta.setId(validaProposta.getId());
			return new ResponseEntity<Proposta> (propostaRepository.save(proposta), HttpStatus.OK);
		}
		
		return new ResponseEntity<Proposta> (propostaRepository.save(proposta), HttpStatus.OK);
	}
	
	@RequestMapping(path="/proposta/cliente/{cpfCliente}", method=RequestMethod.GET)
	@ResponseBody
	public Proposta getPropostaByCpfCliente(@PathVariable String cpfCliente) {	
		
		return propostaRepository.findByCpfCliente(cpfCliente);
	}
	
	@RequestMapping(path="/proposta/concluir", method=RequestMethod.POST)
	@ResponseBody
	@CrossOrigin
	public ResponseEntity<?>  concluirProposta(@RequestBody Proposta proposta) 
	{
		proposta.setStatus(true);
		Proposta validaProposta = propostaRepository.findByCpfClienteAndStatus(proposta.getCpfCliente(), false);
		
		if (validaProposta != null) {
			proposta.setId(validaProposta.getId());
		}
		
		// Aqui deveriamos chamar a fila! Já que concluido irá para fila!
		return new ResponseEntity<Proposta> (propostaRepository.save(proposta), HttpStatus.OK);
	}
	
}
